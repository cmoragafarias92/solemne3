package root;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.*;
import org.json.JSONArray;

/**
 *
 * @author carlos
 */
public class Solemne3Controller {
    
    
    String url;
    JSONArray jsonArray;
    Response apiResponse;
 
    public void setUrl (String url){
        this.url = url;
    
    }
    
    
    public JSONArray getArrayResponse (){
        Client client = ClientBuilder.newClient();
        apiResponse = client.target(url).request().get();
        jsonArray = new JSONArray(apiResponse.readEntity(String.class));
        return jsonArray;
    
    }
    
    
    
    
}