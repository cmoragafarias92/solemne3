<%-- 
    Document   : edit.jsp
    Created on : Dec 18, 2019, 6:09:06 PM
    Author     : carlos
--%>

<%@page import="org.apache.http.HttpResponse"%>
<%@page import="org.apache.http.impl.client.HttpClientBuilder"%>
<%@page import="org.apache.http.entity.StringEntity"%>
<%@page import="org.apache.http.entity.ContentType"%>
<%@page import="org.apache.http.client.HttpClient"%>
<%@page import="org.apache.http.client.HttpClient"%>
<%@page import="org.apache.http.client.methods.HttpPut"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar</title>
    </head>
    
 <body style="background-color: black">
         
         <h2 style="height:80px">
             <span class="container" style="color: aquamarine;position: absolute">Editar registro</span> 
         </h2>
         <span class="container"style="position: absolute;width: 100%;height: 100%"> 
                 

                 <form id="editForm">
                     <div style="width: 100px; text-align:left;">
                         <div style="padding:15px;color: aquamarine">
                             Marca <input name="brandForm" placeholder=${param.brand} />
                         </div>
                         <div style="padding:10px;color: aquamarine">
                             Modelo <input name="modelForm" placeholder=${param.model} />
                         </div>
                         
                          <div style="padding:10px;color: aquamarine">
                              <input  name="id" type="hidden" value=${param.idd} />
                         </div>

                         <div style="padding:20px;text-align:center">
                             <input type="submit" value="Guardar cambios" />
                         </div>
                     </div>
                 </form>
              
                     
       
                         
                         
    <%
       
       
       String brand = request.getParameter("brandForm");
       String model = request.getParameter("modelForm");
       String urlString = "https://phones-bd-test.herokuapp.com/api/phones/";
       
        if (brand != null && model != null){
            //HttpClient httpClient = new DefaultHttpClient(); 

            HttpClient httpClient = HttpClientBuilder.create().build(); //Use this instead 
            HttpPut requestt = new HttpPut(urlString);
        StringEntity params = new StringEntity("{\"id\":" +request.getParameter("id")+",\"brand\":\"" + brand + "\", \"model\":\"" + model + "\"}",ContentType.APPLICATION_JSON);
            
        System.out.println("{\"id\":" +request.getParameter("id")+",\"brand\":\"" + brand + "\", \"model\":\"" + model + "\"}");
            requestt.setEntity(params);
    
            HttpResponse rresponse = httpClient.execute(requestt);
            
            response.sendRedirect("\\index.jsp");
             
            }

  %>
         </span> </body>

</html>
